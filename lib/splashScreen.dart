import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:woof_woof/homePage.dart';

class splashScreen extends StatefulWidget {
  const splashScreen({Key? key}) : super(key: key);

  @override
  _homePageState createState() => _homePageState();
}

class _homePageState extends State<splashScreen> {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 10,
      navigateAfterSeconds: homePage(),
      title: Text(
        'ANIMALIA',
        style: TextStyle(
          fontSize: 40,
          fontWeight: FontWeight.bold,
          color: Colors.red,
        ),
      ),
      image: Image.asset('assets/puppy.png'),
      photoSize: 180,
      backgroundColor: Colors.white,
      loaderColor: Colors.red,
      loadingText: Text(
        'made by Gautam Anand',
        style: TextStyle(
          color: Colors.red,
          fontSize: 16.0,
        ),
      ),
    );
  }
}
