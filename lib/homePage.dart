import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:tflite/tflite.dart';
import 'main.dart';

class homePage extends StatefulWidget {
  const homePage({Key? key}) : super(key: key);

  @override
  _homePageState createState() => _homePageState();
}

class _homePageState extends State<homePage> {

  bool isWorking = false;
  String result='';
  CameraController? cameraController;
  CameraImage? imgCamera;

  initCamera() {
    cameraController = CameraController(cameras![0], ResolutionPreset.medium);
    cameraController!.initialize().then((value) {
      if (!mounted) {
        return;
      }

      setState(() {
        cameraController!.startImageStream((image)=>
        {
          if(!isWorking)
            {
              isWorking=true,
              imgCamera=image,
              runModelOnStream(),
            }
        });

      });
    }
    );
  }

  // "   "+(element["confidence"] as double).toStringAsFixed(2)+"\n\n
  loadModel() async{
    await Tflite.loadModel(model: "assets/model.tflite",labels: "assets/label.txt");

  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadModel();
  }

  runModelOnStream() async{
    if(imgCamera!=null)
    {
      var recognitions = await Tflite.runModelOnFrame(
          bytesList: imgCamera!.planes.map((plane) {return plane.bytes;}).toList(),// required
          imageHeight: imgCamera!.height,
          imageWidth: imgCamera!.width,
          imageMean: 127.5,   // defaults to 127.5
          imageStd: 127.5,    // defaults to 127.5
          rotation: 90,       // defaults to 90, Android only
          numResults: 3,      // defaults to 5
          threshold: 0.1,     // defaults to 0.1
          asynch: true        // defaults to true
      );

      result='';
      recognitions!.forEach((element) {
        result+=element["label"];
      });
    }
    setState(() {
      result;
    });
    isWorking=false;
  }

  @override
  void dispose() async{
    // TODO: implement dispose
    super.dispose();
    await Tflite.close();
    cameraController?.dispose();


  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        child: Scaffold(
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/monitor.png'),
              ),
            ),
            child: Column(
              children: [
                Stack(
                  children: [
                    Center(
                      child: Container(
                        color: Colors.black12,
                        height: 300,
                        width: 300,
                        // child: Image.asset('assets/mob.png'),
                      ),
                    ),
                    Center(
                      child: TextButton(
                        onPressed: ()
                        {
                          initCamera();
                        },
                        child: Container(
                          margin: EdgeInsets.only(top: 35),
                          height: 500,
                          width: 500,
                          child: imgCamera==null?Container(
                            height: 270,
                            width: 360,
                            child: Icon(Icons.photo_camera_front,color: Colors.blue,size: 40,),
                          ):AspectRatio(aspectRatio: cameraController!.value.aspectRatio,
                            child: CameraPreview(cameraController!),),
                        ),
                      ),
                    ),
                  ],
                ),
                Center(
                  child: Container(
                    color: Colors.red,
                    margin: EdgeInsets.only(top: 55.0),
                    child: SingleChildScrollView(
                      child: Text(
                        result,
                        style: TextStyle(
                          backgroundColor: Colors.red,
                          fontSize: 25.0,
                          color: Colors.black,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
